package dao;

import javax.persistence.EntityManager;
import model.Funcionario;
import persistencia.ConexaoJPA;

public class FuncionarioJPA {

	public void salvarFuncionario(Funcionario funcionario) {

		EntityManager em = ConexaoJPA.getInstance().getFactory().createEntityManager();
		em.getTransaction().begin();
		em.merge(funcionario);
		em.getTransaction().commit();

		em.close();

	}

	public boolean logarFuncionario(String registro, String senha) {

		try {
			EntityManager em = ConexaoJPA.getInstance().getFactory().createEntityManager();
			Funcionario f = em
					.createNamedQuery("Login.Control", Funcionario.class)
					.setParameter("registro", registro)
					.setParameter("senha", senha).getSingleResult();

			if (f != null) {
				return true;
			}
			return false;
		} catch (Exception e) {
			return false;
		}
	}

}

package dao;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import model.Especialidade;
import persistencia.ConexaoJPA;

public class EspecialidadeJPA {

	public void salvarEspecialidade(Especialidade especialidade) {

		EntityManager em = ConexaoJPA.getInstance().getFactory().createEntityManager();
		em.getTransaction().begin();
		em.merge(especialidade);
		em.getTransaction().commit();

		em.close();

	}

	public void removerEspecialidade(int id) {

		Especialidade especialidade;

		EntityManager em = ConexaoJPA.getInstance().getFactory().createEntityManager();
		em.getTransaction().begin();
		especialidade = em.find(Especialidade.class, id);
		em.remove(especialidade);
		em.getTransaction().commit();
		em.close();
		
		
	}

	public List<Especialidade> listarEspecialidades() {

		EntityManager em = ConexaoJPA.getInstance().getFactory().createEntityManager();
		em.getTransaction().begin();

		Query consulta = em.createQuery("select especialidade from Especialidade especialidade");
		List<Especialidade> especialidades = consulta.getResultList();
		return especialidades;
	}
}

package dao;

import javax.persistence.EntityManager;
import model.Paciente;
import persistencia.ConexaoJPA;

public class PacienteJPA {

	public void salvarPaciente(Paciente paciente) {

		EntityManager em = ConexaoJPA.getInstance().getFactory().createEntityManager();
		em.getTransaction().begin();
		em.merge(paciente);
		em.getTransaction().commit();

		em.close();

	}
}

package view;

import java.util.List;
import java.util.Scanner;
import dao.EspecialidadeJPA;
import dao.FuncionarioJPA;
import dao.PacienteJPA;
import model.Especialidade;
import model.Funcionario;
import model.Paciente;

public class TelaPrincipal {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);
		Funcionario funcionario = new Funcionario();
		Paciente paciente = new Paciente();
		Especialidade especialidade = new Especialidade();
		FuncionarioJPA operarFuncionario = new FuncionarioJPA();
		PacienteJPA operarPaciente = new PacienteJPA();
		EspecialidadeJPA operarEspecialidade = new EspecialidadeJPA();
		
		int acao;

		System.out.println("*** Bem vindo ao Safira Hospital Center *** \n");

		System.out.println("** Que tipo de usu�rio voc� �? 1- Paciente ou 2- Funcion�rio ** \n");
		int user = input.nextInt();

		if (user == 1) {
			
			System.out.println("\n ** Fa�a login no sistema para prosseguir **\n");

			System.out.println("Informe o n�mero do seu localizador: ");
			int localizadorDoPaciente = input.nextInt();

			System.out.println("Informe o n�mero do seu prontu�rio: ");
			int prontuarioDoPaciente = input.nextInt();

			System.out.println(" ** Escolha uma das op��es abaixo para prosseguir ** \n");

			System.out.println("\n 1- Marcar consulta \n");
			System.out.println("\n 2- Desmarcar consulta \n");
			System.out.println("\n 3- Listar consultas marcadas \n");
			System.out.println("\n 4- Listar faltas em consultas marcadas \n");
			System.out.println("\n 5- Buscar hist�rico \n");

		}

		if (user == 2) {
			
			System.out.println("\n ** Digite 1 para se cadastrar no sistema ou 2 para fazer login **\n");
			int opcao = input.nextInt();
			input.nextLine();
			
			if(opcao == 1){
				
				System.out.println("Informe seu nome completo: ");
				String nomeDoFuncionario = input.nextLine();
					
				System.out.println("Informe seu email: ");
				String emailDoFuncionario = input.nextLine();
				
				System.out.println("Informe o n�mero do seu registro de identifica��o: ");
				String NovoRegistroDoFuncionario = input.nextLine();
				
				System.out.println("Informe sua senha: ");
				String registroDoFuncionario = input.nextLine();
				
				funcionario.setNome(nomeDoFuncionario);
				funcionario.setEmail(emailDoFuncionario);
				funcionario.setRegistro(NovoRegistroDoFuncionario);
				funcionario.setSenha(registroDoFuncionario);
				
				operarFuncionario.salvarFuncionario(funcionario);
				
			}
			
			else if(opcao == 2){
				
				System.out.println("Informe o n�mero do seu registro: ");
				String registroDoFuncionario = input.next();

				System.out.println("Informe sua senha: ");
				String senhaDoFuncionario = input.next();
				
				funcionario.setRegistro(registroDoFuncionario);
				funcionario.setSenha(senhaDoFuncionario);
				
				operarFuncionario.logarFuncionario(registroDoFuncionario, senhaDoFuncionario);
				
				System.out.println("\n ** Escolha uma das op��es abaixo para prosseguir ** \n");
				
			
				do{
				
					System.out.println("\n 1- Fazer cadastro de novo paciente \n");
					System.out.println("\n 2- Inserir especialidade dispon�vel \n");
					System.out.println("\n 3- Remover especialidade \n");
					System.out.println("\n 4- Listar todas as especialidades dispon�veis do m�s \n");
					System.out.println("\n 5- Buscar pacientes marcados por data \n"); //n�o fazer agora, depende da tabela consultas que ser� preenchida pelo paciente ao marcar consultas (parte de s�rgio)
					System.out.println("\n 0- Sair \n");
			
					acao = input.nextInt();
					input.nextLine();
					
					switch (acao) {
					
					case 1:
						
						System.out.println("\n Nome: \n");
						String nomeDoPaciente = input.nextLine();
						
						System.out.println("\n Idade: \n");
						int idadeDoPaciente = input.nextInt();
						input.nextLine();
						
						System.out.println("\n Sexo: \n");
						String sexoDoPaciente = input.nextLine();
						
						System.out.println("\n Email: \n");
						String emailDoPaciente = input.nextLine();
						
						System.out.println("\n Telefone \n");
						int telefoneDoPaciente = input.nextInt();
						input.nextLine();
						
						System.out.println("\n CPF: \n");
						int cpfDoPaciente = input.nextInt();
						input.nextLine();
						
						System.out.println("\n Identidade: \n");
						int identidadeDoPaciente = input.nextInt();
						input.nextLine();
						
						System.out.println("\n Matr�cula: \n");
						int matriculaDoPaciente = input.nextInt();
						input.nextLine();
						
						System.out.println("\n Localizador: \n");
						String localizadorDoPaciente = input.nextLine();
						
						paciente.setNome(nomeDoPaciente);
						paciente.setIdade(identidadeDoPaciente);
						paciente.setSexo(sexoDoPaciente);
						paciente.setEmail(emailDoPaciente);	
						paciente.setTelefone(telefoneDoPaciente);
						paciente.setCpf(cpfDoPaciente);
						paciente.setIdentidade(identidadeDoPaciente);
						paciente.setMatricula(matriculaDoPaciente);
						paciente.setLocalizador(localizadorDoPaciente);
						
						operarPaciente.salvarPaciente(paciente);
						
						break;
						
					case 2:
				
						System.out.println("\n Nome da especialidade: \n");
						String nomeDaEspecialidade = input.nextLine();
						
						System.out.println("\n Nome do profissional que ir� atender essa especialidade: \n");
						String nomeDoProfissional = input.nextLine();
					
						especialidade.setEspecialidade(nomeDaEspecialidade);
						especialidade.setProfissional(nomeDoProfissional);
						
						operarEspecialidade.salvarEspecialidade(especialidade);
						
						break;
						
					case 3:
						System.out.println("\n Informe o ID da especialidade: \n");
						int idDaEspecialidade = input.nextInt();
						operarEspecialidade.removerEspecialidade(idDaEspecialidade);
						
						break;
					
					case 4:
						System.out.println("\n Listando especialidades... \n");
						
						List<Especialidade> especialidades = operarEspecialidade.listarEspecialidades();
		            	
		            	for(Especialidade especialidadee : especialidades){
		            		System.out.println(especialidadee.getEspecialidade());
		            	}
						break;
						
					}
				}while(acao != 0);
				
				
		}

		
	}

}
}

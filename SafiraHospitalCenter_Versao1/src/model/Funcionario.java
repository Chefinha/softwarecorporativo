package model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name= "funcionario") //nomenclatura da tabela
public class Funcionario implements Serializable{

		@Id 
		@GeneratedValue(strategy = GenerationType.IDENTITY) //auto-incrementado
			
		@Column
		private int id;
		
		@Column
		private String nome;
		
		@Column
		private String email;
		
		@Column
		private String registro;
		
		@Column
		private String senha;
		
		

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public String getNome() {
			return nome;
		}

		public void setNome(String nome) {
			this.nome = nome;
		}

		public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}

		public String getRegistro() {
			return registro;
		}

		public void setRegistro(String registro) {
			this.registro = registro;
		}

		public String getSenha() {
			return senha;
		}

		public void setSenha(String senha) {
			this.senha = senha;
		}
		
		
		
		
}

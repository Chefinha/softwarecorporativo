package persistencia;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;


public class ConexaoJPA {

	private static String PERSISTENCE_UNIT_NAME = "hospital";
	private EntityManagerFactory factory;
	private static ConexaoJPA instance = null;

	private ConexaoJPA()
	{
		factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
	}

	public EntityManagerFactory getFactory()
	{
		return factory;
	}

	public static ConexaoJPA getInstance()
	{
		if (instance == null)
		{
			instance = new ConexaoJPA();
		}
		return instance;
	}

	private void destroyFactory()
	{
		factory.close();
	}

	public static void destroyInstance()
	{
		if (instance != null)
		{
			instance.destroyFactory();
			instance = null;
		}
	}
}